const db = require("../models");
const Movies = db.movies;
const Op = db.Sequelize.Op;

// Create and Save a new movie
exports.create = (req, res) => {
    // Validate request
    // Adding a list of movies
    if(Array.isArray(req.body)){
      req.body.map((element)=>{
        //console.log(element)
        element.Metascore = element.Metascore === 'NA'? null : element.Metascore
        element.Gross_Earning_in_Mil = element.Gross_Earning_in_Mil === 'NA'? null : element.Gross_Earning_in_Mil
        let data = {
          Rank: element.Rank,
          Title: element.Title,
          Description: element.Description, 
          Runtime: element.Runtime,
          Genre: element.Genre,
          Rating: element.Rating,
          Metascore: element.Metascore,
          Votes: element.Votes,
          Gross_Earning_in_Mil: element.Gross_Earning_in_Mil,
          Director: element.Director,
          Actor: element.Actor,
          Year: element.Year
        };
        //console.log(data)
        Movies.create(data)
        .then(data => {
          res.status(201).send()
        })
        .catch(err => {
          res.status(500)
          res.send({
            message:"Some error occurred while creating the movie."
          });
        });
        })
    }else if (!req.body.Rank) {
      res.status(400)
      res.send({
        message: "Content can not be empty!"
      });
    } else{ 
      let Metascore = req.body.Metascore === 'NA'? null : req.body.Metascore
      let Gross_Earning_in_Mil = req.body.Gross_Earning_in_Mil === 'NA'? null : req.body.Gross_Earning_in_Mil
      const movies = {
        Rank: req.body.Rank,
        Title: req.body.Title,
        Description: req.body.Description, 
        Runtime: req.body.Runtime,
        Genre: req.body.Genre,
        Rating: req.body.Rating,
        Metascore: Metascore,
        Votes: req.body.Votes,
        Gross_Earning_in_Mil: Gross_Earning_in_Mil,
        Director: req.body.Director,
        Actor: req.body.Actor,
        Year: req.body.Year
      };
      // Save movie in the database
      Movies.create(movies)
        .then(data => {
          res.status(201).send(data);
        })
        .catch(err => {
          res.status(500)
          res.send({
            message:
              err.message || "Some error occurred while creating the movie."
          });
        });
    }
}
// Retrieve all movies from the database.
exports.findAll = (req, res) => {  
    Movies.findAll()
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500)
        res.send({
          message:
            err.message || "Some error occurred while retrieving movies."
        });
      });
  };

// Find a single movie with an rank
exports.findOne = (req, res) => {
    const rank = req.params.Rank;
  
    Movies.findByPk(rank)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500)
        res.send({
          message: "Error retrieving movie with rank=" + rank
        });
      });
  };

// Update a movie by the rank in the request
exports.update = (req, res) => {
    const rank = req.params.Rank;
  
    Movies.update(req.body, {
      where: { rank: rank }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "movie was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update movie with rank=${rank}. Maybe movie was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500)
        res.send({
          message: "Error updating movie with rank=" + rank
        });
      });
  };

// Delete a movie with the specified rank in the request
exports.delete = (req, res) => {
  const rank = req.params.Rank;

  Movies.destroy({
    where: { Rank: rank }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "movie was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete movie with rank=${rank}. Maybe movie was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500)
      res.send({
        message: "Could not delete movie with rank=" + rank
      });
    });
};

// Delete all movies from the database.
exports.deleteAll = (req, res) => {
  Movies.destroy({
    where: {},
    truncate: false
  }).then(nums => {
      res.send({ message: `${nums} movies were deleted successfully!` });
    })
    .catch(err => {
      res.status(500)
      res.send({
        message:
          err.message || "Some error occurred while removing all movies."
      });
    });
};

// find all published movie
exports.findAllYear = (req, res) => {
  Movies.findAll({ where: { Year: req.params.Year }})
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500)
      res.send({
        message:
          err.message || "Some error occurred while retrieving movies."
      });
    });
};

exports.findAllTitle = (req, res) => {
  Movies.findAll({ where: { Title: {[Op.regexp]:`^${req.params.Title}$` } }})
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500)
      res.send({
        message:
          err.message || "Some error occurred while retrieving movies."
      });
    });
};

exports.findAllGenre = (req, res) => {
  Movies.findAll({ where: { Genre : {[Op.regexp]:`^${req.params.Genre}$` } }})
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500)
      res.send({
        message:
          err.message || "Some error occurred while retrieving movies."
      });
    });
};