const db = require("../models");
const User = db.user;
const Op = db.Sequelize.Op;
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
require('dotenv').config()

// Create and Save a new movie
exports.create = async (req, res) => {
    try {
    const user = { User: req.body.User, Password: await bcrypt.hash(req.body.Password, 10) }
    User.create(user)
      .then(data => {
        res.status(201).send('Uploaded');
      })
      .catch(err => {
        res.status(500)
        res.send({
          message:
            err.message || "Some error occurred while creating the movie."
        });
      });
    //res.status(201).send()
  } catch {
    res.status(500).send({
        message: "Content can not be empty!"
      });
  }
}

// Find a single movie with an rank
exports.findOne = (req, res) => {
    const user = req.body.User;
  
    User.findByPk(user)
      .then((data) => {
        if (user == null) {
            return res.status(400).send('Cannot find user')
          }
        try {
            bcrypt.compare(req.body.Password, data.Password)
            .then(bool=>{
            if(bool){
                const accesstoken = jwt.sign(data.User,process.env.ACCESS_TOKEN_SECRET)
                res.json({accessToken:accesstoken})
            } else {
              res.send('Not Allowed')
            }})
          } catch {
            res.status(500).send()
          }
      })
      .catch(err => {
        res.status(500)
        res.send({
          message: "Error retrieving movie with user=" + user
        });
      });
  };
