module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("user", {
        User: {
            type : Sequelize.STRING,
            primaryKey: true
        },
        Password: {
            type : Sequelize.STRING
        },  
    },
    {
       timestamps: false
    });    
    return User;
};