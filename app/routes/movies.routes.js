module.exports = app => {
    const movies = require("../controllers/movies.controller.js");
    const jwt = require('jsonwebtoken')
  
    let router = require("express").Router();
  
    // Create a new movie
    router.post("/",authenticateToken, movies.create);
  
    // Retrieve all movies
    router.get("/",authenticateToken, movies.findAll);
  
    // Retrieve all published movies
    router.get("/Year/:Year",authenticateToken, movies.findAllYear);
    router.get("/Title/:Title",authenticateToken, movies.findAllTitle);
    router.get("/Genre/:Genre",authenticateToken, movies.findAllGenre);
    // Retrieve a single movie with id
    router.get("/:Rank",authenticateToken, movies.findOne);
  
    // Update a movie with id
    router.put("/:Rank",authenticateToken, movies.update);
  
    // Delete a movie with id
    router.delete("/:Rank",authenticateToken, movies.delete);
  
    // Delete all movies
    router.delete("/",authenticateToken, movies.deleteAll);
  
    const user = require("../controllers/user.controller.js");
    
    router.post("/login/create", user.create);
    
    router.post("/login/", user.findOne);

    function authenticateToken(req, res, next) {
      const authHeader = req.headers['authorization']
      const token = authHeader && authHeader.split(' ')[1]
      if (token == null) return res.sendStatus(401)
    
      jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        console.log(err)
        if (err) return res.sendStatus(403)
        req.user = user
        next()
      })
    }

    app.use('/', router);
  };
  