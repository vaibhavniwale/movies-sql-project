const express = require("express")
const bodyParser = require("body-parser")
const cors = require("cors")
const fs = require('fs')

const app = express();

let corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("../models");

db.sequelize.sync();
// // drop the table if it already exists
/*db.sequelize.sync({ force: true }).then(() => {
  console.log("Drop and re-sync db.");
})*/
require("../routes/movies.routes")(app);


// set port, listen for requests
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
